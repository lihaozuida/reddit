(function(){
    
    var ht = ht || {};
    $.extend(ht, {
        setcookie: function(key, value, expireMin) {
            var exdate = new Date();
            exdate.setTime(exdate.getTime() + expireMin*60*1000);
            document.cookie = key +  "=" + escape(value) + (!!expireMin ? ";expires="+exdate.toGMTString() : "");
        }
        , getcookie: function(key){
            var arr = document.cookie ? document.cookie.split(';') : [],
                res = '',
                unit = '';
            $.each(arr, function(k, v){
                unit = $.trim(v).split('=');
                if(unit.length>1 && unit[0] === key) {
                    return res = unit[1] || '';
                }
            });
            return res;
        }
        , queryString: function(key){
            var r = location.href.match(new RegExp('[?&]?'+key+'=[0-9a-zA-Z%._-]*[^&]', 'g'));
            r = r && r[0] ? (r[0][0]=='?' || r[0][0]=='&' ? r[0].slice(1) : r[0]) : '';
            return r.slice(key.length+1);
        }
    });

    window.ht = ht;
}());
