$(function(){
    
    var index = ({
        init: function(){
            this.getDom();
            this.dataInit();
            this.bind();
        }
        , getDom: function(){
            this.index = $('.ht-content');
            this.pages = this.index.find('.ht-pages');
            this.pre = this.pages.find('a').eq(0);
            this.next = this.pages.find('a').eq(1);
        }
        , dataInit: function(){
            var me = this;
            
            if(me.pre.attr('href') == 'javascript:;') {
                me.pre.removeClass('ht-pl');
            }
            if(me.next.attr('href') == 'javascript:;') {
                me.next.removeClass('ht-pl');
            }
        }
        , bind: function(){
            var me = this;
            
            this.index.delegate('.ht-list-up, .ht-list-down', 'click', function(){
                var that = $(this),
                    v = isNaN(Number(that.html())) ? '' : Number(that.html()),
                    id = that.parents('li').data('guid');

                v !== '' && !ht.getcookie('ht_'+id) && that.html(++v) && ht.setcookie('ht_'+id, 1);

                that.hasClass('ht-list-up') ? $.post('/up') : $.post('/down');
            });   
        }

    }.init());


    
}());
