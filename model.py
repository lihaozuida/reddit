__author__ = 'lihao'

import web

db = web.database(dbn='mysql', db='ranker', user='admin', pw='mypass', host='db', port=3306)


def get_contents(s=1, n=25):
    return db.select('ranker_content', limit="%d,%d" % (s, n))


def up_content(content_id):
    return db.query('update ranker_content set up=up+1 where content_id=%d limit 1' % content_id)


def down_content(content_id):
    return db.query('update ranker_content set down=down+1 where content_id=%d limit 1' % content_id)

