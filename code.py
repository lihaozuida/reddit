__author__ = 'lihao'

import web
import model

import json

### Url mappings
urls = (
    '/(\d+)', 'index',
    '/up/(\d+)', 'up',
    '/down/(\d+)', 'down'
)

### Templates
render = web.template.render('templates')


class index:
    def GET(self, p):
        p = int(p)
        n = 25
        s = (p - 1) * n
        contents = model.get_contents(s, n)

        current_page = 1
        if p > 1:
            current_page = p - 1
        prev_page_url = '/%d' % current_page

        next_page_url = '/%d' % (current_page + 1)

        return render.index(contents, prev_page_url, next_page_url)


class up:
    def POST(self, content_id):
        content_id = int(content_id)
        affect_rows = model.up_content(content_id)
        if affect_rows == 1:
            result = {'code': 0, 'message': '', 'data': None}
        else:
            result = {'code': 1001, 'message': 'up failed', 'data': None}
        return json.dumps(result)


class down:
    def POST(self, content_id):
        content_id = int(content_id)
        affect_rows = model.down_content(content_id)
        if affect_rows == 1:
            result = {'code': 0, 'message': '', 'data': None}
        else:
            result = {'code': 1001, 'message': 'down failed', 'data': None}
        return json.dumps(result)


if __name__ == "__main__":
    app = web.application(urls, globals())
    app.internalerror = web.debugerror
    app.run()
