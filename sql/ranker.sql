CREATE TABLE `ranker_content` (
  `content_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `description` varchar(512) NOT NULL,
  `url` varchar(512) NOT NULL,
  `image_url` varchar(512) NOT NULL,
  `content_md5` char(32) NOT NULL,
  `create_time` datetime NOT NULL,
  `up` int(11) unsigned NOT NULL,
  `down` int(11) unsigned NOT NULL,
  PRIMARY KEY (`content_id`),
  UNIQUE KEY `content_md5` (`content_md5`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='main content of ranker'